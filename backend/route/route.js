const express = require('express');
const { inscription, connexion, message, media, commentaire, deleteCompte, deletePost, discussionView, commentaireView, compteView } = require('../controllers/post');
const router = express.Router();
const rateLimit = require('express-rate-limit');

const auth = require('../middleware/auth');
const multer = require('../middleware/multer_config');

const creatAccountLimiter = rateLimit({
    windowMs: 60 * 60 * 1000,
    max: 20,
    message: 'tentative de connexion depassee reessayer dans 1 heure! merci'
})

router.post('/inscription', inscription)
router.post('/connection',creatAccountLimiter, connexion)
router.post('/discussion',auth,  message)
router.post('/discussion/file',auth, multer, media)
router.post('/commentaire',auth,  commentaire)
router.delete('/discussion/:prenom/compte',auth,  deleteCompte)
router.delete('/discussion/:idPost',auth, deletePost)
router.get('/discussion', discussionView)
router.get('/discussion/commentaire/:id',auth,  commentaireView)
router.get('/discussion/:prenom/compte',auth, compteView)

module.exports = router;