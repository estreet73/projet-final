const express = require('express');
const path = require('path');
const route = require('./route/route');
const helmet = require("helmet");

const app = express();

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS, SENT');
    next();
  });

app.use('/image', express.static(path.join(__dirname, 'image')));  
app.use(express.json());
app.use('/api', route);
app.use(helmet());



module.exports = app;