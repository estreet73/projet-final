const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const sql = require('mysql');
const fs = require('fs');
require('dotenv').config()

const sqlConfig = sql.createConnection( {
    user : process.env.DB_USER,
    password : process.env.DB_PASS,
    database : process.env.DB_NAME,
    server : process.env.DB_HOST
});

exports.inscription = (req,res,next) =>{

    bcrypt.hash(req.body.identifiant.passWord,10)
        .then(function(hash)  {
            sqlConfig.query(`INSERT into inscription(nom,prenom,email,password)
                            VALUES ('${req.body.identifiant.nom}',
                            '${req.body.identifiant.prenom}'
                            ,'${req.body.identifiant.email}','${hash}')`
                            ,(err,results) =>{
                            if(err) throw err;     
            })
        })
        .catch(err => {console.log(err)})
}

exports.connexion = (req,res,next) =>{
    
    sqlConfig.query(`SELECT * from inscription WHERE email in 
                    ('${req.body.connecter.email}') `, (err,results) =>{
                        console.log(results);
                       
                        bcrypt.compare(req.body.connecter.passWord, results[0].password)
                             .then(valid =>{
                                if(!valid){
                                  console.log('mot de passe invalide!!');
                                  res.json({connect:false})
                                }if(valid){
                                    res.status(200).json({
                                    connect : true,
                                    prenom: results[0].prenom,
                                    id: results[0].id,
                                    token : jwt.sign(
                                        {userId: results[0].id},
                                        'RANDOM_TOKEN_SECRET',
                                        {expiresIn: '24h'}
                                        )
                                    })
                                }
                             })            
                    })                         
}

exports.message = (req,res,next) =>{

    sqlConfig.query(`INSERT into discution(message,date,prenom) 
    VALUES ('${req.body.message}', now(), '${req.body.identifiant}' )`, (err,results)=>{
        
    
  })
  res.send('post enregistre !')
  
}

exports.media = (req,res,next) =>{

    const img = (req.file)
    const fileName = `${req.protocol}://${req.get('host')}/image/${img.filename}`;
    
    sqlConfig.query(`INSERT into discution(prenom,img_url,date) VALUES 
    ('${req.body.identifiant}', '${fileName}', now() )`, (err,results)=>{
        
    })
    
    res.send('file enregistre')
}

exports.commentaire = (req,res,next) =>{

    sqlConfig.query(`INSERT into commentaires(id,com) VALUES
    (' ${req.body.message.id}' ,'${req.body.com}')`, (err,results)=>{
        if(results){
            console.log('ok com');
        }else{
            console.log(err);
        }
    })
    res.send('com pris en compte')
    
}

exports.deleteCompte = (req,res,next) =>{

    sqlConfig.query(`DELETE from inscription WHERE id IN ('${req.params.id.slice(1)}' )`, (err,results)=>{
        if(results){
             console.log('compte supprimer');
             
         }else{
             console.log(err);
         }
 
     })
     res.send('compte supprimer !')
    
}

exports.deletePost = (req,res,next) =>{
    sqlConfig.query(`SELECT * from discution WHERE id IN ('${req.params.idPost.slice(1)}')`, (err,results)=>{
        let post = JSON.stringify(results)
        postJson = JSON.parse(post)
        if(postJson[0].img_url){
            const fileName = postJson[0].img_url.split('/image/')[1];
            fs.unlink(`image/${fileName}`, ()=>{
                console.log('fichier effacer');
            })
        } 
    })
     
    sqlConfig.query(`DELETE from discution WHERE id IN ('${req.params.idPost.slice(1)}')`, (err,results)=>{
        
        if(results){
            console.log('post supprimer');
           
        }else{
            console.log(err);
        }
    })
    res.send('post supprimer')
    next()

}

exports.discussionView = (req,res,next) =>{

    sqlConfig.query('SELECT * from discution ORDER BY date DESC ', (err, results) =>{

        if(err){
            return res.status(400).send(err + 'erreur de server')
        }
        if(results) {
            return res.status(200).json({
                 results
            })
        }
    } )
   
}

exports.commentaireView = (req,res,next) =>{

    sqlConfig.query(`SELECT * FROM commentaires WHERE id IN (${req.params.id.slice(1)})`, (err, results)=>{
        
        if(err){
           return res.send(err)
        }else{
            return res.json(results)
        }
    } )
   
}

exports.compteView = (req,res,next) =>{

    sqlConfig.query(`SELECT * FROM inscription WHERE prenom IN ('${req.params.prenom.slice(1)}')`, (err, results)=>{
       
        if(err){
            return res.send(err)
         }else{
             return res.json(results)
         }
    })

}