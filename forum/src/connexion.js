import React, { useState } from "react";
import Form from 'react-bootstrap/Form';
import {LinkContainer} from "react-router-bootstrap";
import axios from "axios";
import { useHistory } from "react-router-dom";
import Button from "react-bootstrap/esm/Button";
import {FiSend} from 'react-icons/fi';


function Connexion ()  {
    
      const [email, setEmail] = useState('');
      const [passWord, setPassWord] = useState('');
      const history = useHistory()               

    const handleValidez = (e) =>{
       
        let connecter = {email, passWord} 
        console.log(connecter);
         
        axios.post("http://localhost:4000/api/connection",{connecter})
              
        .then(data => {
            
             let connect = data.data.connect
             console.log(data.data);
             
             if(connect === true){
                
              history.push(`/discussion/:${data.data.prenom}/:${data.data.id}`)
              localStorage.setItem('token', `bearer ${data.data.token}`)
              
             
             }else{
                
                 alert('identifiant inconnu') 
             }
        })
        .catch(error  =>{
            console.log(error)
        })
              
    }


    return(
     
    <div>    
    <h1 className="mt-4">Bienvenue a Groupomania !</h1>
    <Form className="container mt-6">
        <Form.Group controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" onChange={(e) =>setEmail( e.target.value)}  />
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" onChange={(e) =>setPassWord( e.target.value)} />
        </Form.Group>
        <Button className="bout p-2" onClick={(e) =>handleValidez(e) }>
                Valider <FiSend />
        </Button>
        
    </Form>

    <div className="mt-4">
     <LinkContainer exact to="/inscription">
        <p className="bout text-center text-white ">Inscrivez vous si vous n avez pas encore de compte !</p>
     </LinkContainer>
    </div>
    </div>
    )
}
export default Connexion;

