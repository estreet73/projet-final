import React, { useState } from "react";
import { useParams } from "react-router-dom"
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
import axios from "axios";
import DiscussionListe from './discussionListe';
import Deconnect from "./deconnect";
import MonCpte from "./monCpte";
import {FiSend} from 'react-icons/fi';
import {GiTalk} from 'react-icons/gi';
import Drop from "./fileDrop";

function Discussion () {

  const [message, setMessage] = useState('');
  const { prenom } = useParams();
  const { id } = useParams();
  const [ajout, setAjout] = useState(0)
  const identifiant = prenom.slice(1)
  const userId = id.slice(1)
  
  const handleValide = (e) =>{

    e.preventDefault();
    axios.post("http://localhost:4000/api/discussion", {message, identifiant},{headers:{'authorization': localStorage.getItem('token') }} )
    .then(data => console.log(data.config.data))
    .catch(err => console.log(err))
    setMessage('')
    setAjout(ajout +1)
    
  }

  const handleChange = (e) =>{
    setMessage(e.target.value)
    
  }

  const handlePostFile = () =>{
    
    setAjout(ajout +1)
   
  }

    return(
    <>
    <div className='container-fluid xs-6 md-4 '>
      <div className='row' >
        <MonCpte prenom={prenom} id={id} />
        <Deconnect />
      </div>
    </div>
    <InputGroup className="container fluid mt-4">
    <InputGroup.Prepend>
      <InputGroup.Text className='bout text-white'  ><GiTalk /></InputGroup.Text>
    </InputGroup.Prepend>
    <FormControl as="textarea" aria-label="With textarea"
     onChange={handleChange} value={message }        
    />
    
    <Button className='bout' onClick={handleValide}>Valider <FiSend /></Button>
   </InputGroup> 
   <div className='container fluid mt-4'>
     <Drop className=' ml-4' identifiant={identifiant} handlePostFile={handlePostFile} />
   </div>  
   
   <div className='container fluid mt-4'> 
         <DiscussionListe  identifiant={prenom} ajout={ajout} id={userId}
            />  
   </div>
   </>
  )
};

export default Discussion;