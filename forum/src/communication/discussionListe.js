import React, { useEffect, useState } from "react";
import Card from 'react-bootstrap/Card';
import axios from "axios";
import Commenter from "./dialogue/com/commenter";
import ButtonAdmin from "./dialogue/buttonAdmin";

function DiscussionListe (props){

    const [post, setPost] = useState([]);
    const [postDelete, setPostDelete] = useState(0)
    const ajout = props.ajout

    useEffect(() =>{
        
      handleAffichePost()
    },[])

    useEffect(() =>{
        
      handleAffichePost()
    }, [ajout, postDelete])

    const handleDeletePost = () =>{
      setPostDelete(postDelete +1)
      
    } 
    
    const handleAffichePost = () =>{

      axios.get("http://localhost:4000/api/discussion", {headers:{'authorization': localStorage.getItem('token') }})
      
      .then((data) =>{
        
        setPost(data.data.results)
      })
      .catch(error =>{console.log(error + 'pas de reponse')})
      
  }
    
    return(
    
     <> 
    {post.map((message) =>{

      let text;
       if(message.message){
         text = message.message
       }if(message.img_url){
         text = <img src={message.img_url} alt={message.img_url} className='col md-auto' />
       }
         
        return(
           <>
            <Card
                
                text={'white'}
                style={{ width: '100%', fontSize: '20px', fontFamily: 'cursive' }}
                className="mb-4 container-fluid carte"
            >
                <Card.Header >
                {message.date.replace('T', '    ').replace('.000Z', '')}
                <span className='ml-4' > </span>
               <h4>{message.prenom}</h4> 
               <div align='end'>
                 <ButtonAdmin id={props.id} idPost={message.id} handleDeletePost={handleDeletePost} />
               </div>
                </Card.Header>
                <Card.Body className='justify-content'>
                <Card.Text key={message.id} >
                  {text}
                <Commenter message={message} />
                </Card.Text>
                </Card.Body>
            </Card>
            
         </>
        )
    })}
            
    </>
    )
}
    
export default DiscussionListe;