import {MdAccountCircle} from 'react-icons/md';
import Button from 'react-bootstrap/Button';
import { useHistory } from "react-router-dom"

function MonCpte (props){

    const prenom = props.prenom
    const userId = props.id

    const history = useHistory();

    const handleCompte = (e) =>{
        e.preventDefault();
        history.push(`/discussion/:${prenom.slice(1)}/:${userId.slice(1)}/compte`)
    
      }

    return(

        <Button className='mt-4 ml-4 mr-5 col bout '   onClick={handleCompte} >
       Mon compte <MdAccountCircle />
        </Button>
    )
}

export default MonCpte;