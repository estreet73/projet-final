import axios from 'axios';
import { useCallback, useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import Button from 'react-bootstrap/esm/Button';
import Card from 'react-bootstrap/Card';
import {IoMdReturnLeft} from 'react-icons/io'

function Compte (){

    const {prenom} = useParams();
    const [nom , setNom] = useState('');
    const [prenomCpte, setPrenomCpte] = useState('');
    const [email, setEmail] = useState('');
    const history = useHistory()
    const {id }= useParams()
    
    const handleDelete = () =>{

        axios.delete(`http://localhost:4000/api/discussion/:${id.slice(1)}/compte`)
        .then(data => console.log(data))
        alert('compte supprimer !!')
        history.push('/inscription')
    }

    const handleInfo = useCallback( () =>{
       return axios.get(`http://localhost:4000/api/discussion/:${prenom.slice(1)}/compte`,{headers:{'authorization': localStorage.getItem('token') }})
        .then(data =>{
            
            setNom(data.data[0].nom)
            setPrenomCpte(data.data[0].prenom)
            setEmail(data.data[0].email)
        })
    },[prenom])

    const handleRetour = () =>{
        history.push(`/discussion/:${prenom.slice(1)}/:${id.slice(1)}`)
    }

    useEffect(() =>{

        handleInfo()
    },[handleInfo])

    return(
        <>
        <Button className='mt-2 ml-2 bout'  size="sm" onClick={handleRetour} >
        Retour
        <IoMdReturnLeft />
        </Button>
       <div className='container fluid text-center' >
       <Card
        bg='dark'
        text={'dark' === 'light' ? 'dark' : 'white'}
        style={{ width: '18rem' }}
        className="mb-2 mt-4 container fluid text-center"
    >
        <Card.Header>Mon compte</Card.Header>
        <Card.Body>
        <Card.Title> </Card.Title>
        <Card.Text>
            <p>nom = {nom}</p>
            <p>prenom = {prenomCpte}</p>
            <p>email = {email}</p>
        </Card.Text>
        </Card.Body>
        </Card>
        <Button variant="danger" onClick={handleDelete} >ME DESINSCRIRE </Button>
       </div>
       </>
    )
} 

export default Compte;