import axios from "axios";
import Button from "react-bootstrap/Button";
import {MdDeleteForever} from 'react-icons/md';

function ButtonAdmin (props){
    
    const handleDelete = () =>{
       
        axios.delete(`http://localhost:4000/api/discussion/:${props.idPost}`,{headers:{'authorization': localStorage.getItem('token') }})
        .then(data => console.log(data))
        .catch(err => console.log(err))
        props.handleDeletePost()
    }

    if(props.id == 27){

        return(

            <Button className='bout' onClick={handleDelete}>
             Supprimer <MdDeleteForever />
           </Button>
        )
    }else{
        return null
    }
         
        
    }


export default ButtonAdmin;