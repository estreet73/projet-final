import React, { useState } from "react";
import Button from "react-bootstrap/esm/Button";
import ComVue from './comVue';
import {FaCommentDots} from 'react-icons/fa';
 
  function Comments(props) {
    const [modalShow, setModalShow] = useState(false);
 
    return (
      <>
        <Button className='bout' onClick={() => setModalShow(true)}>
          Voir <FaCommentDots />
        </Button>
  
        <ComVue
          show={modalShow}
          onHide={() => setModalShow(false)}
          message={props.message}
        />
      </>
    );
  }
  
  export default Comments;