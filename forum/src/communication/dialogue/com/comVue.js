import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { useEffect } from 'react';
import axios from 'axios';
import { useState } from 'react';

function ComVue(props) {

  const [commentaire, setCommentaire] = useState([]);
  const messageId = props.message.id;
  
  const handleRecup = ()=>{
    axios.get(`http://localhost:4000/api/discussion/commentaire/:${messageId}`,{headers:{'authorization': localStorage.getItem('token') }})
    .then((data )=>{
      
      setCommentaire(data.data)
    })
  }
  
  useEffect(()=>{
    
    handleRecup()
  }, [props.show])

  
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Commentaires
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {commentaire.map((com)=>{
            return(
              <div key={com.com} className='paraf'>
                {com.com}
              </div>
            )
          })}
          
        </Modal.Body>
        <Modal.Footer>
          <Button className='bout' onClick={props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }

    export default ComVue;