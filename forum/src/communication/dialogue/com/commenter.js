import React,{ useState } from 'react';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
import Commentaires from "./commentaires";
import axios from 'axios';
import {FiSend} from 'react-icons/fi';


function Commenter (props){

    const [com, setCom] = useState('');
    const message = props.message;

    const handleChange = (e) =>{
        setCom( e.target.value)
        
      }

      const handleClick = (e) =>{
        e.preventDefault()
        axios.post('http://localhost:4000/api/commentaire', {com, message},{headers:{'authorization': localStorage.getItem('token') }} )
        .then(data =>{console.log(data)})
        .catch(err =>{console.log(err)})
        setCom('')
      }

    return(
        <>
        <div className='mt-4 p-5'>
        <InputGroup className="container fluid mt-4">
        <FormControl as="textarea" aria-label="With textarea"
            placeholder='commenter' onChange={handleChange} 
            value={com} size='sm' 
        />
        <Button className='bout' onClick={handleClick} >Valider <FiSend /> </Button>
        </InputGroup> 
        </div>
        <div >
            <Commentaires message={message} />
        </div>
        </>

    )

}

export default Commenter;