import {FaPowerOff} from 'react-icons/fa';
import { useHistory } from "react-router-dom";
import Button from 'react-bootstrap/Button';

function Deconnect (){

    const history = useHistory();

    const handleDeconnexion = (e) =>{

        localStorage.clear()
        history.push('/')
        
      }

    return(

        <Button  className='mt-4 mr-5 offset-5 bout col ' onClick={handleDeconnexion} >
        Deconnection <FaPowerOff/>
        </Button>
    )
}

export default Deconnect;