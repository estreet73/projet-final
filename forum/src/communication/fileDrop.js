import { FileDrop } from 'react-file-drop';
import axios from 'axios';
import Button from 'react-bootstrap/Button'
import {AiFillCheckCircle} from 'react-icons/ai'
import { useState } from 'react';

function Drop (props){

    const identifiant = props.identifiant;
    const [img, setImg] = useState('')

    const onFileInputChange = (event) => {
        const { files } = event.target;
        setImg(files)
      }

    const onClick = () => {
        
        let formData = new FormData()
        formData.append('file', img[0])
        formData.append('identifiant', identifiant)
    
        axios.post('http://localhost:4000/api/discussion/file', formData,  {headers:{
            'authorization': localStorage.getItem('token'), 'content-type': 'multipart/form-data'}})
            .then(res => console.log(res))
            .catch(err => console.log(err))
        props.handlePostFile()
    }
    
    return(
        <>
        <FileDrop>
            <input type='file' onChange={onFileInputChange} />
            <Button onClick={onClick} className='bout'>Valider <AiFillCheckCircle/></Button>
        </FileDrop>
           
        </>

    )
}

export default Drop;
