import React from "react";
import Nav from "../routage/navBar";
import Connexion from "../connexion";
import Discussion from "../communication/discussion";
import Inscription from "../inscription";
import {Route} from "react-router-dom";
import Compte from "../communication/Compte";


function forum (){
        return(
                <>
                <Nav />
                <Route path="/" exact render={() => <Connexion />} />
                <Route path="/discussion/:prenom/:id" exact render={() => <Discussion />} />
                <Route path="/inscription" exact render={() => <Inscription />} />
                <Route path="/discussion/:prenom/:id/compte" exact render={()=> <Compte /> } />
                </>
        )
}
export default forum;
 