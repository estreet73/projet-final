import React from "react";
import Navbar from 'react-bootstrap/Navbar';
import Nav from "react-bootstrap/Nav";
import {LinkContainer} from "react-router-bootstrap"
import ImgLogo from "../image/icon-above-font.png";




const navbar = (props) =>(
<>
    <Navbar className='carte' variant='dark' expand="lg">
        <Navbar.Brand href="#home"><img src={ImgLogo} alt="logo Groupamonia" width="100px" height="auto" ></img></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
                <LinkContainer exact to="/">
                    <Nav.Link >Connexion</Nav.Link>
                </LinkContainer>
                <LinkContainer exact to="/inscription">
                    <Nav.Link >Inscription</Nav.Link>
                </LinkContainer>
               
            </Nav>
        </Navbar.Collapse>
    </Navbar>
</>
)

export default navbar;
