import React, {Component} from "react";
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import {LinkContainer} from 'react-router-bootstrap';
import axios from 'axios'
import { FiSend } from "react-icons/fi";

class inscription extends Component{

  state={
    email : '',
    nom :'',
    prenom :'',
    passWord : ''
  }

  handleValide = (e) => {

    let identifiant = {...this.state}
    
    axios.post('http://localhost:4000/api/inscription',{identifiant} )
    .then(response => console.log(response))
    .catch(err => console.log(err))
  }

  render(){
 return( 
<>
    <h1>Inscription au forum de discussion</h1>

    <Form className="container">
  <Form.Group controlId="formBasicEmail">
    <Form.Label>E-mail</Form.Label>
    <Form.Control type="email" placeholder=" Email" onChange={(e) => this.setState({email: e.target.value})}  />
    
  </Form.Group>
  <Form.Group controlId="nom">
    <Form.Label>Nom</Form.Label>
    <Form.Control type="text" placeholder="Nom" onChange={(e) => this.setState({nom: e.target.value})} />
    
  </Form.Group>
  <Form.Group controlId="Prenom">
    <Form.Label>Prenom</Form.Label>
    <Form.Control type="texte" placeholder="Prenom" onChange={(e) => this.setState({prenom: e.target.value})} />
    
  </Form.Group>
  <Form.Group controlId="password">
    <Form.Label>Mot de passe</Form.Label>
    <Form.Control type="password" placeholder="Mot de passe" onChange={(e) => this.setState({passWord: e.target.value})} />
    
  </Form.Group>
  
  <LinkContainer exact to="/">
  <Button className='bout' type="submit" onClick={(e)=> {this.handleValide()} } >
   Valider <FiSend />
  </Button>
  </LinkContainer>
</Form>
</>
 );
}
}
export default inscription;